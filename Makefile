NAME = sepia_filter

CFLAGS     = -g -O2 -Wall -Werror -Wignored-qualifiers -Wincompatible-pointer-types -Wint-conversion -Isolution/include/ -std=c17 -pedantic -Wall -Werror -ggdb -Wno-attributes
CC = clang
LINKER = $(CC)
ASM        = nasm
ASMFLAGS   =-felf64 -g

dir_guard = $(shell [ ! -d $(@D) ] && mkdir -p $(@D))

SOLUTION_DIR = solution
BUILDDIR = build

SRCDIR = $(SOLUTION_DIR)/src
INCDIR = $(SOLUTION_DIR)/include
OBJDIR_SSE = obj/sse
OBJDIR_NAIVE = obj/naive

SOURCES_C = $(wildcard $(SRCDIR)/*.c) $(wildcard $(SRCDIR)/*/*.c)
SOURCES_ASM =  $(wildcard $(SRCDIR)/*.asm)
TARGET_SSE  := $(BUILDDIR)/$(NAME)_sse
TARGET_NAIVE  := $(BUILDDIR)/$(NAME)_naive


OBJECTS_SSE := $(SOURCES_C:$(SRCDIR)/%.c=$(OBJDIR_SSE)/%.o) $(SOURCES_ASM:$(SRCDIR)/%.asm=$(OBJDIR_SSE)/%.o)
OBJECTS_NAIVE := $(SOURCES_C:$(SRCDIR)/%.c=$(OBJDIR_NAIVE)/%.o) $(SOURCES_ASM:$(SRCDIR)/%.asm=$(OBJDIR_NAIVE)/%.o)

all: $(TARGET_SSE) $(TARGET_NAIVE)

$(TARGET_SSE): $(OBJECTS_SSE)
	$(dir_guard)
	$(LINKER) $(CFLAGS) $(OBJECTS_SSE) -o $@

$(OBJDIR_SSE)/%.o: $(SRCDIR)/%.c
	$(dir_guard)
	$(CC) $(CFLAGS) -M -MP $< >$@.d
	$(CC) $(CFLAGS) -DUSE_SEPIA_SSE -c $< -o $@

$(OBJDIR_SSE)/%.o: $(SRCDIR)/%.asm
	$(dir_guard)
	$(ASM) $(ASMFLAGS) -o $@ $<



$(TARGET_NAIVE): $(OBJECTS_NAIVE)
	$(dir_guard)
	$(LINKER) $(CFLAGS) $(OBJECTS_NAIVE) -o $@

$(OBJDIR_NAIVE)/%.o: $(SRCDIR)/%.c
	$(dir_guard)
	$(CC) $(CFLAGS) -M -MP $< >$@.d
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJDIR_NAIVE)/%.o: $(SRCDIR)/%.asm
	$(dir_guard)
	$(ASM) $(ASMFLAGS) -o $@ $<

clean: 
	rm -rf $(BUILDDIR) $(OBJDIR_SSE) $(OBJDIR_NAIVE)

aboba:
	$(info sse is $(OBJECTS_SSE))

.PHONY: clean

