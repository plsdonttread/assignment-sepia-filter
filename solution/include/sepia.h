#ifndef SEPIA_H
#define SEPIA_H

#include "image.h"

struct image sepia_naive(struct image src);
struct image sepia_sse(struct image src);


#endif
