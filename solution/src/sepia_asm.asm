section .rodata
    align 16
    c1: dd 0.131, 0.168, 0.189, 0.131
        dd 0.543, 0.686, 0.769, 0.543
        dd 0.272, 0.349, 0.393, 0.272
    
    align 16
    c2: dd 0.168, 0.189, 0.131, 0.168
        dd 0.686, 0.769, 0.543, 0.686
        dd 0.349, 0.393, 0.272, 0.349

    align 16
    c3: dd 0.189, 0.131, 0.168, 0.189
        dd 0.769, 0.543, 0.686, 0.769
        dd 0.393, 0.272, 0.349, 0.393

section .text
global sepia_asm

filter:
    movdqa xmm0, [rdi]
    movdqa xmm1, [rdi+16]
    movdqa xmm2, [rdi+32]
    movdqa xmm3, [rsi]
    movdqa xmm4, [rsi+16]
    movdqa xmm5, [rsi+32]
    mulps xmm3,  xmm0
    mulps xmm4,  xmm1
    mulps xmm5,  xmm2
    addps xmm5, xmm3
    addps xmm5, xmm4
    cvtps2dq xmm5, xmm5
    packssdw xmm5, xmm5
    packuswb xmm5, xmm5
    movd [rdx], xmm5
    ret

; rdi - original color values (4 colors)
; rsi - matrix offset, later used as a pointer to matrix values
; rdx - resulting buffer
sepia_asm:
    cmp rsi, 0
    jne .second_chunk
    mov rsi, c1
    jmp .set
    .second_chunk:
    cmp rsi, 1
    jne .third_chunk
    mov rsi, c2
    jmp .set
    .third_chunk:
    mov rsi, c3

    .set:
    jmp filter