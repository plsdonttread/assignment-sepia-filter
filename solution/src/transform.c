#include <stdio.h>

#include "transform.h"

struct image rotate(const struct image src) {
	struct image new = image_new(src.height, src.width);
    for(size_t y = 0; y < src.height; ++y) {
    	for(size_t x = 0; x < src.width; ++x) {
			*pixel_of(new, src.height-1-y, x) = *pixel_of(src, x, y);
    	}
    }	

	return new;
}

