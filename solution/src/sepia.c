#include <stdio.h>
#include <string.h>

#include "sepia.h"
#include "image.h"

static unsigned char sat( uint64_t x ) {
  if ( x < 256 ) return x; return 255;
}

static float byte_to_float[256];
static inline void fill_lookup() {
  for(uint16_t i = 0; i < 256; i++) {
    byte_to_float[i] = i;
  }
}

static void sepia_one( struct pixel *const pixel ) {
  static const float c[3][3] = {
     { .393f, .769f, .189f },
     { .349f, .686f, .168f },
     { .272f, .543f, .131f }
  };
  struct pixel const old = *pixel;
  pixel->r = sat(
    byte_to_float[old.r] * c[0][0] + 
    byte_to_float[old.g] * c[0][1] + 
    byte_to_float[old.b] * c[0][2]);
  pixel->g = sat( 
    byte_to_float[old.r] * c[1][0] + 
    byte_to_float[old.g] * c[1][1] + 
    byte_to_float[old.b] * c[1][2]);
  pixel->b = sat(
    byte_to_float[old.r] * c[2][0] + 
    byte_to_float[old.g] * c[2][1] + 
    byte_to_float[old.b] * c[2][2]);
}

struct image sepia_naive(struct image src) {
  fill_lookup();
	struct image new = image_copy(&src);
  uint32_t x, y;
  for ( y = 0; y < new.height; y++ )
    for ( x = 0; x < new.width; x++) {
		sepia_one( pixel_of( new, x, y ) ); 
	} 

	return new;
}

extern void sepia_asm(float colors[12], uint8_t offset, uint8_t res[4]);

static void prepare_colors(float prepared[3][12], struct pixel pixels[4]) {
  float buf[3][12];
  for(size_t i = 0; i < 4; i++) {
    for(size_t j = 0; j < 3; j++) {
      buf[0][i*3+j] = byte_to_float[pixels[i].b];
      buf[1][i*3+j] = byte_to_float[pixels[i].g];
      buf[2][i*3+j] = byte_to_float[pixels[i].r];
    }
  }
  for(size_t i = 0; i < 3; i++) {
    for(size_t j = 0; j < 4; j++) {
      prepared[i][0 + j] = buf[0][i*4+j];
      prepared[i][4 + j] = buf[1][i*4+j];
      prepared[i][8 + j] = buf[2][i*4+j];
    }
  }
}

struct image sepia_sse(struct image src) {
  fill_lookup();
  struct image new = image_copy(&src);
  const size_t size = src.width * src.height;
  uint8_t *res = (uint8_t*)new.data;
  size_t i;
  for(i = 0; i < (size - size % 4); i+=4) {
    float prepared[3][12];
    
    prepare_colors(prepared, src.data+i);
    for(size_t j = 0; j < 3; j++) {       
      sepia_asm(prepared[j], j, res);
      res = res + 4;
    }
  }
  i = (size - size % 4);
  for(; i < size; i++) {
    sepia_one(&new.data[i]);
  }

  return new;
}
